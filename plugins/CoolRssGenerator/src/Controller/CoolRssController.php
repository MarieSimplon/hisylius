<?php

declare(strict_types=1);

namespace Acme\CoolRssPlugin\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
// use Sylius\Bundle\ShopBundle;

final class CoolRssController extends Controller
{
    public function CreateRssAction(?string $name): Response

    // sylius_core:
    // resources:
    //     product_image:
    //         classes:
    //             model: App\Entity\Product\ProductImage

    {  
        $repository = $this->container->get('sylius.repository.product');
        $channel = $this->container->get('sylius.context.channel')->getChannel();
        $locale = $this->container->get('sylius.context.locale')->getLocaleCode();
        // $core = $this->container->get('sylius.context.product')->getImage();
        $count = 5;
        $products = $repository->findLatestByChannel($channel, $locale, $count);
        // $images = $repository->findLatestByChannel($channel, $core);
        return $this->render('@CoolRssPlugin/static_greeting.html.twig', [
            'products' => $products, 
            // 'images'=>$images
            ]);
    }
    // /**
    //  * @param string|null $name
    //  *
    //  * @return Response
    //  */
    // public function staticallyGreetAction(?string $name): Response
    // {
    //     return $this->render('@AcmeSyliusExamplePlugin/static_greeting.html.twig', ['greeting' => $this->getGreeting($name)]);
    // }

    // /**
    //  * @param string|null $name
    //  *
    //  * @return Response
    //  */
    // public function dynamicallyGreetAction(?string $name): Response
    // {
    //     return $this->render('@AcmeSyliusExamplePlugin/dynamic_greeting.html.twig', ['greeting' => $this->getGreeting($name)]);
    // }

    // /**
    //  * @param string|null $name
    //  *
    //  * @return string
    //  */
    // private function getGreeting(?string $name): string
    // {
    //     switch ($name) {
    //         case null:
    //             return 'Hello!';
    //         case 'Lionel Richie':
    //             return 'Hello, is it me you\'re looking for?';
    //         default:
    //             return sprintf('Hello, %s!', $name);
    //     }
    // }
}
