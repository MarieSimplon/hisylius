<?php

declare(strict_types=1);

namespace Acme\CoolRssPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class CoolRssPlugin extends Bundle
{
    use SyliusPluginTrait;
}
