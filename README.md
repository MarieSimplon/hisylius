L'objectif du projet est l'installation d'un framework e-commerce pour découvrir une plateforme de vente de ligne et mettre en oeuvre un composant réutilisable.

# Objectifs pédagogiques

Les savoirs attendus à l'issue du projet :
* Prise en main de sylius
* Créer un plugin sylius

Les compétences visées:
* Compétence 6, développer les composants d'accès aux données : niveau adapter/transposer
* Compétence 8, élaborer et mettre en oeuvre des composants dans une application de gestion de contenu ou e-commerce : niveau adapter/transposer

# Description du projet

Installation de la version standard de Sylius ; une fois l'installation terminée, prenez en main le [back-office de l'application](http://127.0.0.1:8000/admin).
 
Création d'un plugin permettant la diffusion des derniers articles de la boutique sous la forme d'un flux RSS.

Exercices "Sylius"
========================

# Exercice (jour 1)

Création de votre [1er plugin Sylius](https://docs.sylius.com/en/1.5/plugins/creating-plugin.html) :

```php
cd {sylius}
mkdir plugins
cd plugins
composer create-project sylius/plugin-skeleton SyliusMyFirstPlugin
```

Configurer l'autoloader de votre projet sylius pour charger votre plugin :


```
"autoload": {
        "psr-4": {
            "App\\": "src/",
            "Acme\\SyliusExamplePlugin\\": "plugins/SyliusMyFirstPlugin/src/"
        }
    }
```

Ajouter votre plugin au fichier "bundles.php" de votre projet Sylius. Un plugin Sylius est un simple **bundle** Symfony avec une infrastructure intégrée pour les tests utilisant Behat.

Configurer votre application Sylius pour prendre en charge les routes déjà paramétrées dans le squelette de votre plugin et vérifier leur fonctionnement (affichage, chargement du javascript,...).

# Exercice (jour 2)

Création d'une nouvelle fiche produit "Disque dur" ; lorsque ce produit sera disponible sur votre boutique, celui-ci doit pouvoir être commandé avec les options suivantes :

* Capacité : 250GO, 500GO, 1TO
* Couleur : rouge, bleu
________________________
<p align="center">
    <a href="https://sylius.com" target="_blank">
        <img src="https://demo.sylius.com/assets/shop/img/logo.png" />
    </a>
</p>

<h1 align="center">Sylius Standard Edition</h1>

<p align="center">This is Sylius Standard Edition repository for starting new projects.</p>

About
-----

Sylius is the first decoupled eCommerce framework based on [**Symfony**](http://symfony.com) and [**Doctrine**](http://doctrine-project.org). 
The highest quality of code, strong testing culture, built-in Agile (BDD) workflow and exceptional flexibility make it the best solution for application tailored to your business requirements. 
Enjoy being an eCommerce Developer again!

Powerful REST API allows for easy integrations and creating unique customer experience on any device.

We're using full-stack Behavior-Driven-Development, with [phpspec](http://phpspec.net) and [Behat](http://behat.org)

Documentation
-------------

Documentation is available at [docs.sylius.org](http://docs.sylius.org).

Installation
------------

```bash
$ wget http://getcomposer.org/composer.phar
$ php composer.phar create-project sylius/sylius-standard project
$ cd project
$ yarn install
$ yarn build
$ php bin/console sylius:install
$ php bin/console server:start
$ open http://localhost:8000/
```

Troubleshooting
---------------

If something goes wrong, errors & exceptions are logged at the application level:

```bash
$ tail -f var/log/prod.log
$ tail -f var/log/dev.log
```

If you are using the supplied Vagrant development environment, please see the related [Troubleshooting guide](etc/vagrant/README.md#Troubleshooting) for more information.

Contributing
------------

Would like to help us and build the most developer-friendly eCommerce platform? Start from reading our [Contributing Guide](http://docs.sylius.org/en/latest/contributing/index.html)!

Stay Updated
------------

If you want to keep up with the updates, [follow the official Sylius account on Twitter](http://twitter.com/Sylius) and [like us on Facebook](https://www.facebook.com/SyliusEcommerce/).

Bug Tracking
------------

If you want to report a bug or suggest an idea, please use [GitHub issues](https://github.com/Sylius/Sylius/issues).

Community Support
-----------------

Have a question? Join our [Slack](https://slackinvite.me/to/sylius-devs) or post it on [StackOverflow](http://stackoverflow.com) tagged with "sylius". You can also join our [group on Facebook](https://www.facebook.com/groups/sylius/)!

MIT License
-----------

Sylius is completely free and released under the [MIT License](https://github.com/Sylius/Sylius/blob/master/LICENSE).

Authors
-------

Sylius was originally created by [Paweł Jędrzejewski](http://pjedrzejewski.com).
See the list of [contributors from our awesome community](https://github.com/Sylius/Sylius/contributors).
